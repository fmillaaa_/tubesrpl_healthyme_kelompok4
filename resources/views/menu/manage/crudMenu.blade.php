<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    @include('partials.header')
    
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
        @include('partials.navbar')
    </nav>

    <section class="ftco-section">
    	<div class="container">
            <a href="/menu/create" class="btn btn-primary btn-sm my-2" style="border-radius: 9px" >Tambah Menu</a>
            <table class="table">
                <thead class="thead-dark">
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Harga</th>
                    <th scope="col">Kategori</th>
                    <th scope="col">Total Kalori</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                    @forelse ($menu as $key=>$item)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$item->nama}}</td>
                            <td>{{$item->harga}}</td>
                            <td>{{$item->kategori}}</td>
                            <td>{{$item->totalKalori}}</td>
                            <td>
                                <form action="/menu/{{$item->id}}" method="POST">
                                    @csrf
                                    @method('delete')
                                    <a href="/menu/{{$item->id}}/edit" class="btn btn-sm btn-warning" style="border-radius: 9px">Edit</a>
                                    <input type="submit" class="btn btn-sm btn-danger" value="Delete" style="border-radius: 9px">
                                </form>
                            </td>                                                   
                        </tr>                         
                    @empty
                        <tr>
                            <td>Data Kategori Kosong</td>
                        </tr>
                    @endforelse  
                </tbody>
            
              </table>
      </div>
    </section>


</body>

@include('partials.script')
</html>
