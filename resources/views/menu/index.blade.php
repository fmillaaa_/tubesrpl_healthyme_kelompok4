{{-- @extends('layouts.master')
    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
        @include('partials.navbar')
    </nav>
    <section class="ftco-section">
		@include('partial.bestDestination')
    </section>
    @section('content')
        @foreach ($menu as $item)
            <div class="col-md-6 col-lg-3 ftco-animate">
                <div class="product">
                    <a href="#" class="img-prod"><img class="img-fluid" src="{{asset('template/images/product-1.jpg')}}" alt="Colorlib Template">
                        <span class="status">30%</span>
                        <div class="overlay"></div>
                    </a>
                    <div class="text py-3 pb-4 px-3 text-center">
                        <h3><a href="#">Bell Pepper</a></h3>
                        <div class="d-flex">
                            <div class="pricing">
                                <p class="price"><span class="mr-2 price-dc">$120.00</span><span class="price-sale">$80.00</span></p>
                            </div>
                        </div>
                        <div class="bottom-area d-flex px-3">
                            <div class="m-auto d-flex">
                                <a href="#" class="add-to-cart d-flex justify-content-center align-items-center text-center">
                                    <span><i class="ion-ios-menu"></i></span>
                                </a>
                                <a href="#" class="buy-now d-flex justify-content-center align-items-center mx-1">
                                    <span><i class="ion-ios-cart"></i></span>
                                </a>
                                <a href="#" class="heart d-flex justify-content-center align-items-center ">
                                    <span><i class="ion-ios-heart"></i></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    @endsection
 --}}
 <!DOCTYPE html>
 <html lang="en">
   <head>
     <title>Vegefoods - Free Bootstrap 4 Template by Colorlib</title>
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
     
     @include('partials.header')
   </head>
   <body class="goto-here">
         @include('partials.contact')
 
     <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
         @include('partials.navbar')
       </nav>
     <!-- END nav -->
 
     <section id="home-section" class="hero">
           @include('partials.sidebar')
     </section>
 
     <section class="ftco-section">
             @include('partials.shipping')
         </section>
 
         <section class="ftco-section ftco-category ftco-no-pt">
             @include('partials.kategori')
         </section>
 
     <section class="ftco-section">
         {{-- @yield('content') --}}
         
             <div class="container">
                 <div class="row">
                     @foreach ($menu as $item)
                         <div class="col-md-6 col-lg-3 ftco-animate">
                             <div class="product">
                                <a href="#" class="img-prod"><img class="img-fluid" src="{{ asset('gambar/'.$item->foto) }}" alt="Colorlib Template">
                                     <div class="overlay"></div>
                                 </a>
                                 <div class="text py-3 pb-4 px-3 text-center">
                                     <h3><a href="#">{{ $item->nama }}</a></h3>
                                     <div class="d-flex">
                                         <div class="pricing">
                                             <p class="price">Rp.{{ $item->harga }}</p>
                                         </div>
                                     </div>
                                     <div class="bottom-area d-flex px-3">
                                         <div class="m-auto d-flex">
                                            <form action="/add-to-cart/{{ Auth::user()->id }}/{{ $item->id }}" method="post">
                                                @csrf
                                                @method('post')
                                                <button class="btn btn-primary" type="submit">Add to cart</button>
                                            </form>
                                             {{-- <a href="#" class="buy-now d-flex justify-content-center align-items-center mx-1">
                                                 <span><i class="ion-ios-cart"></i></span>
                                             </a> --}}
                                             
                                         </div>
                                     </div>
                                 </div>
                             </div>
                         </div>
                         {{-- <a href="#" class="img-prod"><img class="img-fluid" src="{{ asset('gambar/'.$item->foto) }}" alt="Colorlib Template"> --}}
                     @endforeach
                 </div>
             </div>
     
        
     </section>
         
     <section class="ftco-section testimony-section">
       @include('partials.testimoni')
     </section>
 
     <hr>
 
   <!-- loader -->
   <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>
 
 
   @include('partials.script')
     
   </body>
   @section('script')
    <script>
        //make funtion on button with search document by id "search" then go to link 
        const searchProduct = () => {
            let search = document.getElementById('search').value;
            window.location.href = `/search-products=${search}`;
        }
    </script>
@endsection
 </html>