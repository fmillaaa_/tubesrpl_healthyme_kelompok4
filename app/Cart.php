<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Cart extends Model
{
    
    protected $table = "cart";
    protected $fillable = [
        "user_id",
        "total_product"
    ];

    public function cartItem()
    {
        return $this->hasMany(CartItem::class, 'id');
    }
}
