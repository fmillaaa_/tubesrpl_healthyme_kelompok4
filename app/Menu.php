<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = 'menu';

    protected $fillabel = ['nama','harga','kandunganVitamin','totalGlukosa','totalKalori','totalKarbohidrat','totalProtein','totalLemak','kategori','foto'];
}
