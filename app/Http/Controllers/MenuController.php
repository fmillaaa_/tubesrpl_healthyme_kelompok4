<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu;
use Illuminate\Support\Facades\DB;
use File;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function indexAdmin()
    {
        $menu = Menu::all();
        return view('menu.manage.crudMenu', compact('menu'));
    }

    public function indexAll()
    {
        $menu = Menu::all();
        return view('menu.index', compact('menu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('menu.manage.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'harga' => 'required',
            'kategori' => 'required',
            'kandunganVitamin' => 'required',
            'totalGlukosa' => 'required',
            'totalKalori' => 'required',
            'totalKarbohidrat' => 'required',
            'totalProtein' => 'required',
            'totalLemak' => 'required',
            'foto' => 'required|image|mimes:jpeg,png,jpg,svg|max:2048',
        ],
        [
            'nama.required' => 'inputan nama harus diisi/tidak boleh kosong',
            'harga.required' => 'inputan harga harus diisi/tidak boleh kosong',
            'kategori.required' => 'inputan kategori harus diisi/tidak boleh kosong',
            'kandunganVitamin.required' => 'inputan kandungan vitamin harus diisi/tidak boleh kosong',
            'totalGlukosa.required' => 'inputan total glukosa harus diisi/tidak boleh kosong',
            'totalKalori.required' => 'inputan total kalori harus diisi/tidak boleh kosong',
            'totalKarbohidrat.required' => 'inputan total karbohidrat harus diisi/tidak boleh kosong',
            'totalProtein.required' => 'inputan total protein harus diisi/tidak boleh kosong',
            'totalLemak.required' => 'inputan total lemak harus diisi/tidak boleh kosong',
            'foto.required' => 'inputan foto harus diisi/tidak boleh kosong',
        ]
        );

        $imageName = time().'.'.$request->foto->extension();

        $request->foto->move(public_path('gambar'), $imageName);


        $menu = new Menu;
        $menu->nama = $request->nama;
        $menu->harga = $request->harga;
        $menu->kandunganVitamin = $request->kandunganVitamin;
        $menu->totalGlukosa = $request->totalGlukosa;
        $menu->totalKalori = $request->totalKalori;
        $menu->totalKarbohidrat = $request->totalKarbohidrat;
        $menu->totalProtein = $request->totalProtein;
        $menu->totalLemak = $request->totalLemak;
        $menu->kategori = $request->kategori;
        $menu->foto = $imageName;
        $menu->save();

        return redirect('/crudMenu');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $menu = DB::table('menu')->where('id', $id)->first();
        return view('menu.manage.edit', compact('menu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'harga' => 'required',
            'kategori' => 'required',
            'kandunganVitamin' => 'required',
            'totalGlukosa' => 'required',
            'totalKalori' => 'required',
            'totalKarbohidrat' => 'required',
            'totalProtein' => 'required',
            'totalLemak' => 'required',
            'foto' => 'image|mimes:jpeg,png,jpg,svg|max:2048',
        ]);

        $menu = Menu::findOrFail($id);

        if($request->has('foto')){
            $path = "gambar/";
            File::delete($path . $menu->foto);
            $imageName = time().'.'.$request->foto->extension();
            $request->foto->move(public_path('gambar'), $imageName);
            $menu->foto = $imageName;
        }
        $menu->nama = $request->nama;
        $menu->harga = $request->harga;
        $menu->kandunganVitamin = $request->kandunganVitamin;
        $menu->totalGlukosa = $request->totalGlukosa;
        $menu->totalKalori = $request->totalKalori;
        $menu->totalKarbohidrat = $request->totalKarbohidrat;
        $menu->totalProtein = $request->totalProtein;
        $menu->totalLemak = $request->totalLemak;
        $menu->kategori = $request->kategori;
        $menu->save();

        return redirect('/crudMenu');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $menu = Menu::findorfail($id);
        $menu->delete();

        $path = "gambar/";
        File::delete($path . $menu->foto);

        return redirect('/crudMenu');
    }
}
