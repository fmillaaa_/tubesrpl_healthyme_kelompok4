<?php

use App\Http\Controllers\CartController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts.master');
});

Route::get('/a', function () {
    return view('menu.index');
});


Route::group(['middleware' => ['auth']], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('menu', 'MenuController');
    Route::get('/crudMenu', 'MenuController@indexAdmin');
    Route::get('/DaftarMenu', 'MenuController@indexAll');
    Route::post('/add-to-cart/{user_id}/{menu_id}', [CartController::class, 'addToCart']);
    Route::post('/subtract-quntity/{user_id}/{menu_id}', [CartController::class, 'subtractCartItemQuantity']);
    Route::post('/add-quntity/{user_id}/{menu_id}', [CartController::class, 'addCartItemQuantity']);
    Route::resource('cart', 'CartItemController');
    
    
});

Auth::routes();


